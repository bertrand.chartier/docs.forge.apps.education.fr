---
hide:
  - navigation
  - toc
  - footer
---
![LFCN](assets/images/brigit_et_komit_transparent.png){ .middle align=left }
#Bienvenue sur la Forge des communs numériques éducatifs<br />

_Proposant informations et documentations sur la forge, ce site, EN CONSTRUCTION, est le point d'entrée du projet._

La forge des communs numériques éducatifs, alias LaForgeEdu, regroupe les enseignants, communautés d'enseignants et leurs partenaires qui créent et partagent des logiciels et ressources éducatives libres. 

Elle rassemble à la fois les professeurs développeurs qui y déposent le code de leurs projets (tels que PrimTux, MathALÉA, e-comBox ou La Nuit du Code) mais aussi les professeurs qui choisissent le format texte ou texte formaté (Markdown, LaTeX, etc.) pour éditer et publier du contenu pédagogique.

 Elle repose sur le logiciel libre GitLab. Elle mutualise et valorise les productions des communautés et aide certains de ses projets à passer à l’échelle.

 Et comme on dit chez nous : _L'union fait la forge !_

[Accéder à la forge](https://forge.apps.education.fr/users/sign_in){ .md-button rel="noopener" }

## Faire communauté

La communauté de la forge échange et communique principalement via la messagerie Tchap.

On y trouve plusieurs forums :

- **[LaForgeEdu](https://tchap.gouv.fr/#/room/!fnVhKrpqraWfsSirBK:agent.education.tchap.gouv.fr?via=agent.education.tchap.gouv.fr&via=agent.diplomatie.tchap.gouv.fr&via=agent.collectivites.tchap.gouv.fr)** : forum général d'actualité de la forge et ses projets, porte d'entrée pour découvrir la forge et se présenter (niveau débutant)
- [TEST LaForgeEdu](https://tchap.gouv.fr/#/room/!FMbqDmwePWphQDroBV:agent.education.tchap.gouv.fr?via=agent.education.tchap.gouv.fr&via=agent.diplomatie.tchap.gouv.fr) : forum du groupe de testeurs des projets de la forge, une façon simple et non chronophage de s'impliquer et mieux comprendre ensemble ce qu'est et comment fonctionne une forge (niveau débutant)
- [DEV LaForgeEdu](https://tchap.gouv.fr/#/room/!BXZZsyWklktciNEDbM:agent.education.tchap.gouv.fr?via=agent.education.tchap.gouv.fr&via=agent.diplomatie.tchap.gouv.fr&via=agent.intradef.tchap.gouv.fr) : forum d'entraide technique, d'expertise et de développement des projets de la forge (niveau avancé)
- [IA LaForgeEdu](https://tchap.gouv.fr/#/room/!IpZpqVTFcNrhUNUAJc:agent.education.tchap.gouv.fr?via=agent.education.tchap.gouv.fr) : forum spécifique de professeurs bidouilleurs d'IA qui documentent et partagent le fruit de leurs expérimentations sur la forge (niveau avancé)

## Ressources de LaForgeEdu

Vous trouverez ci-dessous une liste, bien entendu non exhaustive, de quelques projets de la forge. Le site de la DRANE Grenoble vous en propose également [une sélection fort bien présentée](https://dane.web.ac-grenoble.fr/1d-0/la-forge-quelques-pepites).

Vous pouvez également suivre en temps réel l'activité de la forge via [l'explorateur](https://forge.apps.education.fr/explore) et disposer d'une vue d'ensemble sur les projets au moyen de notre [console](https://docs.forge.apps.education.fr/console/).

### Logiciels libres
- **[PrimTux](https://primtux.fr/)** [[source](https://forge.apps.education.fr/primtux)] Libre et gratuit, sécurisé, fonctionnant sur tous les ordinateurs, même anciens, PrimTux est système informatique complet dédié à l’apprentissage et conçu par des pédagogues, pour l’école et la maison (école)
- **[MathALÉA](https://coopmaths.fr/alea/)** [[source](https://forge.apps.education.fr/coopmaths/mathalea/)] Ressources libres en mathématiques pour vidéoprojecteur, smartphone et papier : plus de 2000 exercices à données aléatoires avec paramétrage des variables didactiques pour travailler les automatismes, pour imprimer des devoirs différents à chaque élève, pour évaluer les élèves sur ordinateur, pour donner des liens de révisions avec corrections (école, collège, lycée, mathématiques)
- **[e-comBox](https://www.reseaucerta.org/pgi/e-combox)** [[source](https://forge.apps.education.fr/e-combox/)] Plateforme regroupant des applications métiers essentielles pour développer les compétences numériques des élèves et des étudiants (lycée, supérieur)
- **[La Nuit du Code](https://www.nuitducode.net/)** [[source](https://forge.apps.education.fr/nuit-du-code)] La Nuit du Code est un marathon de programmation durant lequel les élèves, par équipes de deux ou trois, ont 6h pour coder un jeu avec Scratch ou Python (école, collège, lycée, sciences)
- **[QCMCam](https://qcmcam.net/)** [[source](https://forge.apps.education.fr/sebastien.cogez/qcmcam2)] Outil en ligne qui permet d’avoir un retour élève lors d’une évaluation, d’un sondage ou d’un vote, chaque élève disposant d’un marqueur imprimé qui lui est propre, suivant l’orientation 4 réponses sont possibles (école, collège, lycée)
- **[Éducajou](https://educajou.forge.apps.education.fr/)** [[source](https://forge.apps.education.fr/educajou)] Suite d'applications libres pour l'école primaire (école)
- **[myMarkmap](https://myMarkmap.forge.apps.education.fr/)** [[source](https://forge.apps.education.fr/myMarkmap/myMarkmap.forge.apps.education.fr)] Outil pour créer facilement et sans inscription des cartes mentales en ligne  (école, collège, lycée)
- **[Flipbook](https://flipbook.forge.apps.education.fr/)** [[source](https://forge.apps.education.fr/flipbook/flipbook.forge.apps.education.fr)] Outil pour créer facilement un livre numérique que l'on peut feuilleter en ligne (école, collège)
- **[mon-oral.net](https://www.mon-oral.net/)** [[source](https://forge.apps.education.fr/mon-oral)] Une plateforme libre pour la pratique de l’oral au primaire et au secondaire et l’entraînement aux examens (collège, lycée)
- **[CréaCarte](https://lmdbt.forge.apps.education.fr/creacarte/)** [[source](https://forge.apps.education.fr/lmdbt/creacarte)] Outil en ligne pour concevoir et éditer des cartes à imprimer (école, collège, lycée)
- **[ChatMD](https://eyssette.forge.apps.education.fr/chatMD/)** [[source](https://forge.apps.education.fr/eyssette/chatMD)] Un chatbot que vous pouvez configurer par vous-même en Markdown (collège, lycée)
- **[Markpage](https://markpage.forge.apps.education.fr/)** [[source](https://forge.apps.education.fr/markpage/markpage.forge.apps.education.fr)] Outil qui permet de créer facilement un mini site web ou une application pour smartphone, à partir d'un simple fichier en Markdown (collège, lycée)
- **[SituLearn](https://situlearn.univ-lemans.fr/)** [[source](https://forge.apps.education.fr/lium/situlearn)] Projet de recherche visant à aider les enseignants à enrichir leurs sorties pédagogiques avec plusieurs applications (collège, lycée)
- **[Ubisit](https://ubisit.forge.apps.education.fr/ubisit-plan-de-classe-aleatoire)** [[source](https://forge.apps.education.fr/ubisit/ubisit-plan-de-classe-aleatoire)] Créez vos plans de classe et optimisez l’organisation de votre salle de cours (école, collège, lycée)
- **[École Inclusive](https://linuxfr.org/news/ecole-inclusive-une-application-libre-pour-la-prise-en-charge-des-eleves-en-situation-de-handicap)** [[source](https://forge.apps.education.fr/dsoulie/Ecole_inclusive)] Une application libre pour la prise en charge des élèves en situation de handicap  (collège)
- **[ePoc](https://epoc.inria.fr/)** [[source](https://forge.apps.education.fr/epoc/e-poc)] Créer et partager librement votre formation sur mobile, ePoc est composé d’un éditeur permettant la création de contenu pédagogique et d’une application mobile permettant de suivre la formation produite  (collège, lycée)
- **[Planet Alert](http://planetalert.tuxfamily.org/)** [[source](https://forge.apps.education.fr/flenglish/planetalert)] Un monde ludique pour une pédagogie stimulante qui articule le monde numérique (site internet, exercices en ligne, espace de classe en ligne) et le monde réel de la classe  (collège, anglais)
- **[Secret Not Tellable](https://degrangem.forge.apps.education.fr/SecretNotTellable/)** [[source](https://forge.apps.education.fr/DegrangeM/SecretNotTellable)] Un jeu sérieux dans lequel les élèves doivent trouver un mot de passe respectant certaines contraintes … liées au programme de SNT  (lycée, SNT)
- **[MathsMentales](https://mathsmentales.net/)** [[source](https://forge.apps.education.fr/mathsmentales/mathsmentales.forge.apps.education.fr)] Pour travailler les automatismes et le calcul mental en mathématiques (école, collège, lycée)
- **[Cahier de vacances](https://coopmaths.fr/www/cahiers-de-vacances/)** [[source](https://forge.apps.education.fr/coopmaths/cahier-de-vacances)] Destinés aux élèves qui vont entrer en seconde, en première ou en terminale, proposer un document libre et gratuit qui aide les élèves à entretenir en autonomie leur culture mathématique durant la trêve estivale (lycée, mathématiques)
- **[LireCouleur](https://lirecouleur.forge.apps.education.fr/weblc6/)** [[source](https://forge.apps.education.fr/lirecouleur)] Ensemble d'outils destiné à aider les lecteurs débutants à décoder les mots en utilisant les principes de la lecture en couleur (école)
- **[Tableau](https://tableau.ensciences.fr/)** [[source](https://forge.apps.education.fr/thibaultgiauffret/tableau)] Boîte à outils en ligne pour les disciplines scientifiques vouée à être projetée au tableau ou à être utilisée par les élèves sur leur propre ordinateur/tablette (lycée, sciences)
- **[AutoBD](https://educajou.forge.apps.education.fr/autobd/)** [[source](https://forge.apps.education.fr/educajou/autobd)] Un générateur en ligne de bandes dessinées (école, collège)
- **[CSE](https://cse.forge.apps.education.fr/)** [[source](https://forge.apps.education.fr/cse/cse.forge.apps.education.fr)] Un outil pour créer un moteur de recherche personnalisé à partir d'une liste de sites (école, collège, lycée)
- **[Seyes](https://educajou.forge.apps.education.fr/seyes/)** [[source](https://forge.apps.education.fr/educajou/seyes)] Affiche une page quadrillée de type cahier d'écolier et permet d'y ajouter du texte en écriture cursive reliée (école)
- **[Face Privacy](https://faceprivacy.forge.apps.education.fr/app/)** [[source](https://forge.apps.education.fr/faceprivacy/app)] Outil en ligne pour flouter les visages sur les photos (école, collège, lycée)
- **[La Console de LaForgeEdu](https://docs.forge.apps.education.fr/console/)** [[source](https://forge.apps.education.fr/docs/console)] Outil de suivi en temps réel de l'activité de la forge (école, collège, lycée)
- **[apiGeom](https://coopmaths.fr/apigeom/)** [[source](https://forge.aeif.fr/coopmaths/apiGeo)] Application en ligne de géométrie dynamique simple et léger développé par Coopmaths : "l'objectif n'est pas de concurrencer GeoGebra, sa particularité est le parti pris d'une géométrie à la française avec une interface épurée et une manipulation entièrement réalisée en Javascript" (collège, lycée, mathématiques)
- **[L'Atelier des problèmes](https://atelier.appenvie.fr/)** [[source](https://forge.apps.education.fr/remi.gilger-ext/les-apps-maths-en-vie)] Une application de l'assocation M@ths en-vie pour s'entrainer à résoudre des problèmes : gratuit, progressif et structuré, suivi enseignant, aides tutorielles, du CP au CM2, oralisation des aides et énoncés (école, mathématiques)
- **[Labomep](https://labomep.sesamath.net/)** [[source](https://forge.apps.education.fr/sesamath/sesalab)] Plateforme développée par Sésamath permettant aux enseignants de proposer à leurs élèves des exercices d'apprentissage, d'entraînement, d'approfondissement et d'en suivre les résultats (école, collège, lycée, mathématiques)
- **[Instrumenpoche](https://instrumenpoche.sesamath.net/)** [[source](https://forge.apps.education.fr/sesamath/instrumenpoche)] Outil développé par Sésamath permettant de créer et lire des animations d'outils de géométrie :règle, compas, équerre, rapporteur… (école, collège, lycée, mathématiques)
- **[MathGraph](https://www.mathgraph32.org/)** [[source](https://forge.apps.education.fr/sesamath/mathgraph)] Logiciel libre de géométrie, d'analyse et de simulation multiplateforme développé par Yves Biton pour Sésamath (école, collège, lycée, mathématiques)
- **[SACoche](https://sacoche.sesamath.net/)** [[source](https://forge.apps.education.fr/sesamath/sacoche)] Logiciel libre développé par Sésamath de suivi d'acquisition de compétences, pour évaluer par compétences et positionner sur le socle commun (école, collège, lycée)
- **[Cyber-Enquête, sur la piste du Hacker](https://cybersecurite.forge.apps.education.fr/cyber-enquete/)** [[source](https://forge.apps.education.fr/cybersecurite/cyber-enquete)] Jeu en ligne de sensibilisation à la cybersécurité (collège, lycée)

### Ressources éducatives libres
- Le site [Cours de philosophie et formations](https://eyssette.forge.apps.education.fr/cours/) [[source](https://forge.apps.education.fr/eyssette/cours/)] par le professeur de philisophie Cédric Eyssette
- Le site [Première NSI](https://cours-nsi.forge.apps.education.fr/premiere/) [[source](https://forge.apps.education.fr/cours-nsi/premiere)] par les professeurs de NSI Pascal Remy et Charles Poulmaire
- Le site [Terminale NSI](https://fjunier.forge.apps.education.fr/tnsi/) [[source](https://forge.apps.education.fr/fjunier/tnsi)] par le professeur de NSI Frédéric Junier
- Le site [Mon parcours devoirs faits](https://clg_v_vasarely.forge.apps.education.fr/devoirs-faits/mon-parcours/) [[source](https://forge.apps.education.fr/clg_v_vasarely/devoirs-faits)] par le professeur de mathématiques Cyril Iaconelli
- Le site [La TeXiothèque](https://lmdbt.forge.apps.education.fr/latexiotheque/) [[source](https://forge.apps.education.fr/lmdbt/latexiotheque)] par le professeur de mathématiques Cyril Iaconelli
- Le site [Exercices de génétique humaine](https://bio.forge.apps.education.fr/heredite/) [[source](https://forge.apps.education.fr/bio/heredite)] par le professeur de SVT Patrice Hardouin 
- Le site [CodEx : le code par les exercices](https://codex.forge.apps.education.fr/) [[source](https://forge.apps.education.fr/codex/codex.forge.apps.education.fr)] par une communauté de professeurs qui enseignent SNT et NSI
- Le site [Enseigner la SNT](https://ressources.forge.apps.education.fr/snt/) [[source](https://forge.apps.education.fr/ressources/snt)] par une communauté de professeurs qui enseignent SNT
- Le site [Laboratoire de mathématiques Grothendieck](https://college-felicien-joly.forge.apps.education.fr/laboratoire-grothendieck/) [[source](https://forge.apps.education.fr/college-felicien-joly/laboratoire-grothendieck)] par les professeurs de mathématiques du collège Félicien Joly de Fresnes sur Escaut (59)
- Le site [Journée Académique Inspirante Moodle Éléa](https://drane-grenoble.forge.apps.education.fr/jaime/) [[source](https://forge.apps.education.fr/drane-grenoble/jaime)] par la DANE de Grenoble
- Le site [La Doctrine Technique](https://doctrine-technique-numerique.forge.apps.education.fr/) [[source](https://forge.apps.education.fr/doctrine-technique-numerique)] par la DNE
- Le site [Lire en ligne Ada & Zangemann](https://ada-lelivre.fr/) [[source](https://forge.apps.education.fr/nicolas-taffin-ext/ada-lelivre)] par Nicolas Taffin, en partenariat avec la DNE
- Le site [Visite virtuelle du lycée Emmanuel Mounier de Grenoble](https://lycee-mounier-grenoble.forge.apps.education.fr/visite-virtuelle/) [[source](https://forge.apps.education.fr/lycee-mounier-grenoble/visite-virtuelle)] par Bertrand Chartier
- Le site [snt.ababsurdo.fr](https://snt.ababsurdo.fr/) [[source](https://forge.apps.education.fr/paternaultlouis/cours-2-snt)] par Louis Paternault : toute l'année de SNT, clef en main, sous licence CC-by-sa
- Le site [Sujets du Bac en LaTeX](https://bactex.cpierquet.fr/) [[source](https://forge.apps.education.fr/pierquetcedric/sujets-bac-latex)] par le professeur de mathématiques Cédric Pierquet
- Le site [Ressources CPGE](https://antoine.crouzet.education/) [[source](https://forge.apps.education.fr/cpge)] par le professeur de mathématiques Antoine Crouzet
- Le site [Ressources numériques](https://num.forge.apps.education.fr/uporu/) [[source](https://forge.apps.education.fr/num/uporu)] par des enseignants du collègue Uporu
- Le site [Formation à LaForgeEdu](https://nsinormandie.forge.apps.education.fr/2024-intro-forge/) [[source](https://forge.apps.education.fr/nsinormandie/2024-intro-forge)] par l'équipe du PCD NSI de l'académie de Normandie
- Le site [Manuels libres](https://lesmanuelslibres.region-academique-idf.fr/) [[source](https://forge.apps.education.fr/drane-ile-de-france/les-manuels-libres)] par la région académique d'Île-de-France
- Le site [CyberSécurité](https://cybersecurite.forge.apps.education.fr/cyber/) [[source](https://forge.apps.education.fr/cybersecurite/cyber)] par un collectif d'enseignants

## Tutoriels

* [vidéo] [Bien commencer avec la Forge](https://tube-numerique-educatif.apps.education.fr/w/vAMyPdtMNRPe8c4TuqhX1d){:target="_blank" } (comment créer un groupe ou un projet, ajouter et modifier des éléments, publier un site) par Bertrand Chartier (DANE Grenoble)
* [Tutoriels et aides pour l'enseignant qui crée son site avec Python](https://docs.forge.apps.education.fr/tutoriels/pyodide-mkdocs-theme-review/){:target="_blank" }
* [Tutoriels et aides pour l'enseignant qui crée son site simple](https://docs.forge.apps.education.fr/tutoriels/tutoriel-site-simple/){:target="_blank" }
* [Comment créer un site à partir d'un autre ?](https://docs.forge.apps.education.fr/tutoriels/pyodide-mkdocs-theme-review/08_tuto_fork/1_fork_projet/){:target="_blank" }
* [Utiliser la Forge avec VSCodium ou Visual Studio Code](https://forge.apps.education.fr/laurent.abbal/utiliser-la-forge-avec-vscodium-ou-visual-studio-code){:target="_blank" }

## Modèles

Quelques modèles sont fournis pour commencer à utiliser la Forge des Communs Numériques Éducatifs, permettant de réaliser

* [Diaporama en Markdown](https://forge.apps.education.fr/docs/modeles/diaporama-en-markdown){:target="_blank" }
* [Un mini site](https://forge.apps.education.fr/docs/modeles/modele-jekyll){:target="_blank" }
* [Une page HTML à partir d'une source en Markdown](https://forge.apps.education.fr/docs/modeles/modele-pandoc){:target="_blank" }
* [Site web de cours à usage général](https://forge.apps.education.fr/docs/modeles/site-web-cours-general){:target="_blank" }
* [Site web de cours avec exercices Python dans le navigateur](https://forge.apps.education.fr/docs/modeles/pyodide-mkdocs-theme-review){:target="_blank" }
* [Compilation de documents $\LaTeX$](https://forge.apps.education.fr/docs/modeles/latex){:target="_blank" }

![I love la Forge](https://forge.apps.education.fr/docs/visuel-forge/-/raw/main/i-love-la-forge-small.png)

## FAQ

#### Comment nous contacter ?
En nous envoyant un mail à forge-communs-numeriques@ldif.education.gouv.fr

Alternativement, vous pouvez nous signaler les soucis rencontrés [en ouvrant un ticket](https://forge.apps.education.fr/groups/docs/-/issues) ou [par courriel](mailto:forge-apps+guichet+docs-support-623-issue-@phm.education.gouv.fr) 

#### Que contient la forge des communs numériques éducatifs ?
Comme son nom l'indique, la forge contient des ressources pédagogiques, logiciels et ressources éducatives libres, utiles à la communauté scolaire.

#### Qu'est-ce que LaForgeEdu peut faire pour vous ?
- La forge vous propose des dizaines de logiciels et ressources éducatives, créées par et pour la communauté scolaire, sans contrainte d'utilisation et de partage car sous licence libre : vous trouverez sûrement chaussures à votre pied ! On vous encourage à nous rejoindre et participer mais, bien entendu, aucun obligation de vous inscrire à LaForgeEdu pour les utiliser.
- Si vous êtes un enseignant développeur ou une enseignante développeuse, le forge vous offre un espace pour y déposer et partager votre code, un peu comme GitHub mais libre et souverain (voir quelques applications exemples [ci-dessous](https://docs.forge.apps.education.fr/#logiciels-libres)).
- Si vous souhaitez partager du contenu pédagogique sur un site web, LaForgeEdu offre des fonctions d'hébergement et de rédaction collaborative du contenu (voir les sites en exemple [ci-dessous](https://docs.forge.apps.education.fr/#ressources-educatives-libres)).
- En rejoignant la forge, vous pourrez bénéficier de la dynamique communautaire qui se met petit à petit en place, en invitant ses membres à interagir et participer aux projets des uns et des autres.
- Vous pourrez être éventuellement invité.e à des événements pour y présenter votre projet en y rencontrant la communauté (La Journée du Libre Éducatif, Ludovia, Educatech...) et participer à des _forgeathon_, sprint de contribution collective aux projets de la forge
- Vous pourrez également bénéficier de la visibilité de la forge en terme de communication.
- Vous pourrez enfin bénéficier de _l'appel à communs_, dispositif qui se met en place pour accompagner, soutenir et valoriser les projets de la forge et leurs communautés.

#### Qu'est-ce que vous pouvez faire pour LaForgeEdu ?
- Autour de vous, dans vos réseaux, en formation... vous nous aideriez à faire connaitre LaForgeEdu et ses projets, en soulignant notamment leurs spécificités (des _communs numériques_ créés et partagés par les enseignants et leurs communautés)
- Vous pouvez également aider les porteurs de projets à enrichir et améliorer leurs ressources : en interagissant avec eux (rapport de bug, retour d'expériences, tests, suggestion d'améliorations...) voire en collaborant à leurs projets
- Vous pouvez aussi animer avec nous les espaces d'échange et de communication, notamment les forums Tchap dédiés dont [celui d'accueil et d'information générale](https://tchap.gouv.fr/#/room/!fnVhKrpqraWfsSirBK:agent.education.tchap.gouv.fr?via=agent.education.tchap.gouv.fr&via=agent.diplomatie.tchap.gouv.fr&via=agent.collectivites.tchap.gouv.fr).
- Enfin vous pouvez carrément rejoindre l'équipe de LaForgeEdu, ce ne sont pas les besoins qui manquent en animation, documentation, formation, communication, expertise technique, etc. Et nul besoin d'etre un _geek_ pour participer. D'avance merci, si tel est le cas ;) N'hésitez pas à nous contacter à forge-communs-numeriques@ldif.education.gouv.fr

#### Qui peut s'inscrire et participer à la Forge des communs numériques éducatifs ?
La forge est désormais dans [apps.education.fr](https://portail.apps.education.fr/). Apps étant relié à l'annuaire fédéré des profs et des agents, tout le personnel du ministère de l'Éducation nationale peut participer à la forge via son compte Apps. Donc pour qu'une ou un collègue s'inscrive et participe à la forge, il lui faut (si ça n'est déjà fait) simplement [activer son compte Apps](https://portail.apps.education.fr/) avec son mail professionnel (et cliquer sur le service de la forge depuis le portail Apps). Vous pouvez aussi vous connecter directement en passant par [cette page](https://forge.apps.education.fr/users/sign_in).

Mais la forge est également ouverte et accueillante aux membres hors ministère, du moment que les projets sont libres et utiles à notre communauté scolaire. Ces membres peuvent être des enseignants et chercheurs/seuses d'université, autres organisations publiques comme l'Inria, acteurs privés, notamment de l'Edtech, collaborant sur nos projets, associations et acteurs des communs, etc. Ici l'inscription se fait sur un deuxième annuaire et est soumise à une validation préalable (pour des questions de responsabilité et sécurité, pour voir aussi si les projets qui candidatent s'inscrivent bien dans le numérique éducatif libre).

Deux annuaires et donc deux catégories d'utilisateurs cohabitent sur la forge. La catégorie du ministère étant évidemment bien plus grande. Tout ça pour dire aussi que si vous souhaitez inviter des personnes hors du ministère à collaborer sur vos projets, c'est possible.

#### Comment faire pour s'inscrire lorsque l'on n'est pas du ministère de l'Education nationale ?
Si vous n'êtes pas du ministère et souhaitez ouvrir un compte externe sur la forge, merci de nous contacter par mail à l'adresse suivante, en vous présentant succinctement et en justifiant votre candidature (à quels projets libres existants comptez-vous participer ? quels nouveaux projets libres comptez-vous partager avec notre communauté ?). Nous vous répondrons dans les plus brefs délais : forge-communs-numeriques@ldif.education.gouv.fr

#### Quelle licence choisir pour mon projet sur la forge ?

Commençons par rappeler que tout projet de LaForgeEdu est un logiciel **libre** ou une ressource éducative **libre**. Et donc tout projet doit s'accompagner à la racine du dépot d'un fichier LICENSE explicitant la licence choisie.

- Si votre projet est un logiciel libre (i.e. du code) alors il conviendra d'en choisir la licence parmi [cette liste](https://www.data.gouv.fr/fr/pages/legal/licences/) du site data.gouv. Si vous choisissez une licence _permissive_, nous vous suggérons la MIT. Si vous préférez une licence _à réciprocité_ (copyleft), nous vous suggérons la GPLv3. Liens vers le site de l'ANCT pour en savoir plus [sur les licences libres en général](https://licence-libre.incubateur.anct.gouv.fr/) et [sur la distinction importante entre les licences permissives et les licences copyleft](https://licence-libre.incubateur.anct.gouv.fr/licence-libre/le-point-sur-les-licences-libres) en particulier.

- Si votre projet est une ressource éducatives libre (par ex. mis en ligne sous la forme d'un site web), il conviendra d'en choisir la licence parmi les différents types de licence Creative Commons. Nous préférons la CC-BY et nous déconseillons d'introduire la clause ND (car l'éducation est en perpétuelle évolution). Pour influencer votre choix, nous vous suggérons ces trois lectures : le témoignage d'un enseignant [Pourquoi je publie mes travaux sous licence libre](https://ababsurdo.fr/blog/20141119-pourquoi-publier-sous-licence-libre/), ce dialogue issu de blog de la chaire RELIA de l'Unesco [Choisir une licence ouverte, une affaire de goût ou de posture ?](https://chaireunescorelia.univ-nantes.fr/2023/06/14/choisir-une-licence-ouverte-une-affaire-de-gout-ou-de-posture/) et [La connaissance libre grâce aux licences Creative Commons, ou pourquoi la clause « pas d’utilisation commerciale » ne répond pas (forcément) à vos besoins](https://upload.wikimedia.org/wikipedia/commons/0/0b/WMBE-La_connaissance_libre_gr%C3%A2ce_aux_licences_Creative_Commons.pdf) de Wikimédia Belgique.

#### Comment suivre l'actualité de la forge ?
Si vous etes enseignant ou agent de la fonction publique, vous pouvez vous inscrire sur notre [salon Tchap dédié](https://tchap.gouv.fr/#/room/!fnVhKrpqraWfsSirBK:agent.education.tchap.gouv.fr?via=agent.education.tchap.gouv.fr&via=agent.diplomatie.tchap.gouv.fr&via=agent.agriculture.tchap.gouv.fr).

Vous pouvez aussi suivre le compte @LeLibreEdu [sur Mastodon](https://mastodon.mim-libre.fr/@lelibreedu) et/ou [sur X](https://twitter.com/LeLibreEdu).

#### Comment inviter la communauté de LaForgeEdu à contribuer à mon projet ?
Nous vous suggérons d'inviter les membres à ouvrir des tickets (issues) dans l'espace dédié de votre projet (afin de raporter un bug, suggérer une amélioration, etc.). C'est souvent le premier pas vers la collaboration.

Ils peuvent également créer un ticket par mail en utilisant une adresse spécifique et unique par projet (que vous trouverez dans _Paramètres > Général > Service d'assistance > Adresse de courriel à utiliser pour le Service Desk_).

Il est également possible de créer un forum au nom de votre projet sur la messagerie instantanée [Tchap](https://www.tchap.gouv.fr/) et inviter les utilisateurs à y participer.

#### Qui compose la communauté de LaForgeEdu ? Quelle gouvernance de LaForgeEdu ?
LaForgeEdu mutualise et fédère des projets de communs numériques. Mais elle souhaite également être elle-même un **commun numérique** dans son approche, sa culture et sa gouvernance : un _« commun des communs »_ en quelque sorte. Pour reprendre la typologie/posture de [la Fabrique des GéoCommuns de l'IGN](https://www.ign.fr/publications-de-l-ign/institut/kiosque/publications/2023_01_dossier_des_communs.pdf), LaForgeEdu compte 5 principales catégories d'acteurs : le membre, le contributeur, l'opérateur, le sponsor et le garant.

Le/la **membre** y utilise ses ressources, s’exprime dans les forums (Tchap), ouvre des tickets pour rapporter un bug, suggère une amélioration et fait remonter les besoins.
Rôle évidemment fondamental, le/la **contributeur**/trice crée ou participe à un ou plusieurs communs de la forge.
L'**opérateur** est le coeur de pilotage de LaForgeEdu. Il administre le service, rédige sa documentation et anime la communauté. Il est composé de personnes de la DNE (Direction du numérique pour l'éducation du ministère), des DANE de Lyon et de Grenoble, de l'association AEIF et d'enseignants.
La DNE est le **sponsor** de la forge. Elle met à disposition et maintient l'instance GitLab sur laquelle repose la forge. Elle veille à ce que LaForgeEdu soit bien intégrée avec la stratégie globale du numérique éducatif. Elle accompagne et valorise la forge et ses projets en y apportant moyens logistiques, humains et financiers.
Le **garant** est un acteur indépendant qui s’assure que les règles de la communauté sont respectées, en l'occurrence ici la DINUM (via sa mission Logiciels libres et communs numériques et son programme Accélérateur d'Initiatives Citoyennes).

#### Y a-t-il d'autres forges à l'Éducation nationale ?
Oui. C'est pour cela qu'il est incorrect et source de confusion de qualifier notre forge de « forge de l'Éducation nationale ». C'est pour cela aussi que la notre s'appelle « forge des communs numériques éducatifs", nom un peu long mais qui oriente notre forge vers le pédagogique. 

Il existe en effet, et depuis longtemps, une forge "métiers" à destination des informaticiens et équipes DSI du ministère et académies. C'est là qu'on y trouve par exemple des applications (pas forcément libres) comme Santorin (pour la correction de copies dématérialisées) ou Afflenet (affectation des élèves en lycée). Ceux qui l'utilisent la nomment souvent "la forge de l'éducation" ou "la forge de Rennes" (car elle est opérée par un pole national qui se trouve à Rennes). Il est bon de l'avoir à l'esprit car certains appellent notre forge "forge de l'Éducation nationale" ignorant l'existence préalable de cette forge et créant sans le vouloir de la confusion. Cette forge est privée, accueillant des projets pas tous libres, projets techniques et non pédagogiques, et elle est non accessible sans identification.

Il y a aussi "la forge mim-libre" (MIM pour Mutualisation Inter-Ministérielle). Elle est opérée par le Pole de Compétences Logiciels Libres PCLL de Dijon (qui s'occupe aussi de l'administration technique de notre forge). On y trouve des applications (toutes libres pour le coup) comme Eole 3 ou... le portail Apps (pour ceux qui se demandaient où était son code). Elle a vocation à accueillir des applications utiles à tous les ministères (et pas seulement du MEN). A noter que c'est aussi au PCLL qu'on doit l'instance mim-libre de Mastodon qui offre un compte Mastodon aux profs et agents du MEN. Elle est accessible sur https://gitlab.mim-libre.fr/

## On parle de LaForgeEdu

Voici les derniers liens qui mentionnent la forge. [Voir toute la revue de presse](revue-de-presse.md).

--8<-- "docs/revue-de-presse.md:accueil"

## Migration

* [Exporter quelques projets](migration/migration_projet.md){:target="_blank" }

![aeifTOforge](assets/images/aeifTOforge.png){ .middle align=center }

* [Import et synchro GitHub - GitLab](https://forge.apps.education.fr/laurent.abbal/import-et-synchro-github-gitlab){:target="_blank" }