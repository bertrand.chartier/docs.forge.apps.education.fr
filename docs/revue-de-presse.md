# Revue de presse : On parle de LaForgeEdu

<!--
    Les éléments entre [start:accueil] et [end:accueil] apparaissent sur la page d'accueil (comme « Derniers articles parlant de la forge »).  Pour ajouter un nouvel élément :
    - ajoutez le en haut de la liste ;
    - remontez la ligne [end:accueil] d'une ligne, pour faire disparaître le dernier élément de la page d'accueil (il sera toujours présent sur cette page, mais la page d'accueil ne sera pas surchargée par une revue de presse trop longueu.
-->

<!-- --8<-- [start:accueil] -->
- [La forge des communs numériques éducatifs, Co-construire, mutualiser et partager des ressources éducatives libres](https://primabord.eduscol.education.fr/la-forge-des-communs-numeriques-educatifs) Christophe Gilger, Primàbord, octobre 2024
- [Éducajou : des applications libres pour l’école](https://www.libreavous.org/220-enjeux-de-la-vie-privee-sur-internet-et-sur-les-appareils-connectes), émission radio "Libre à vous !", Jean-Christophe Becquet, octobre 2024
- [La forge des communs numériques éducatifs pour mutualiser nos outils et nos ressources](https://drne.region-academique-bourgogne-franche-comte.fr/la-forge-des-communs-numeriques-educatifs-pour-mutualiser-nos-outils-et-nos-ressources/), Stéphane Fontaine, DRANE Bourgogne-Franche-Comté, octobre 2024
- [La Forge des communs numériques : votre nouvel atelier pour innover en classe](https://dane.site.ac-lille.fr/2024/10/03/la-forge-des-communs-numeriques-votre-nouvel-atelier-pour-innover-en-classe/), Rudy Alba, DRANE Hauts-de-France, octobre 2024
- [Les pépites de la forge](https://dane.web.ac-grenoble.fr/1d-0/la-forge-quelques-pepites), Bertrand Chartier, DRANE AURA site Grenoble, septembre 2024
<!-- --8<-- [end:accueil] -->
- [La Forge des communs numériques éducatifs : collaborer, mutualiser, innover](https://outilstice.com/2024/06/forge-des-communs-numeriques-educatifs-collaborer-mutualiser/), Fidel Navamuel (Les Outils Tice), juin 2024
- [podcast] [MADE by ERUN : Arnaud Champollion](https://kiterun.aft-rn.net/made-by-erun-arnaud-champollion/), mai 2024
- [vidéo] Audran Le Baron, [discours de cloture](https://podeduc.apps.education.fr/video/42285-journee-du-libre-educatif-a-luniversite-de-creteil/) de la Journée du Libre Educatif 2024 à Créteil ([transcription](https://www.librealire.org/journee-du-libre-educatif-2024-audran-le-baron)), mars 2024
- [Des ressources libres pour la SNT](https://www.class-code.fr/actualites/2024-02-13-des-ressources-libres-pour-la-snt/), Charles Poulmaire, Class Code, février 2024
- [Forges de l’ESR – Définition, usages, limitations rencontrées et analyse des besoins](https://www.ouvrirlascience.fr/forges-de-lesr-definition-usages-limitations-rencontrees-et-analyse-des-besoins/) - Daniel Le Berre, Jean-Yves Jeannas, Roberto Di Cosmo, François Pellegrini, Ouvrir la Science, mai 2023
- [Développer les communs numériques](https://www.class-code.fr/actualites/2023-05-05-developper-les-communs-numeriques-entretien-avec-alexis-kauffmann-chef-de-projet-a-la-dne/) - Entretien avec Alexis Kauffmann, chef de projet à la DNE, mai 2023, Class Code, mai 2023
- [vidéo] [L’enjeu des forges souveraines et du logiciel libre](https://www.youtube.com/watch?v=8KQGa6U0HAo), par François Élie ([transcription](https://www.librealire.org/l-enjeu-des-forges-souveraines-et-du-logiciel-libre-par-francois-elie)), 2021
