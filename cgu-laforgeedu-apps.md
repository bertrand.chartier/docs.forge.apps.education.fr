# Validation juridique de LaForgeEdu dans Apps

*Version 1.1 17/26 mai écrite par Emma D, relue par Alexis K*

*Objectif : Insérer la forge sur ces 3 pages de projet.apps.education
- https://projet.apps.education.fr/mentions-legales/
- https://projet.apps.education.fr/donnees-personnelles
- https://projet.apps.education.fr/cgu*

## Mentions légales - Forge des communs numériques éducatifs

### Présentation

Ce service, appelé Forge des communs numériques éducatifs, permet à l’utilisateur identifié d'accéder à une forge logicielle, reposant sur le logiciels libre GitLab Community Edition, pour y déposer, collaborer et partager du code et du contenu à vocation pédagogique. Il est réservé en écriture aux personnels de l'éducation nationale et aux partenaires inscrits pour une durée limitée et renouvelable sur un annuaire externe des comptes invités. L’accès en lecture aux ressources de la Forge des communs numériques éducatifs peut être public.

### Identification de l'éditeur

Ministère de l'Éducation nationale et de la Jeunesse
Direction du numérique pour l'éducation
110 rue Grenelle, 75007 Paris 

### Directeur de publication

M. Audran Le Baron, Directeur du numérique pour l’éducation

### Maîtrise d'ouvrage et rédaction

La sous direction du Socle assure la coordination du processus de production, de validation et d'accessibilité des services conçus par la direction du numérique pour l’éducation, les académies ou les institutions et organismes partenaires, ainsi que le développement des applications, l'administration du site et des services et l'accompagnement exécutif de la gouvernance du projet dans le cadre du projet “Services numériques partagés” (SNP).

* Responsable de production : Laurent Le Prieur
* Chef de projet : Benoît Piédallu
* Porteur fonctionnel : Alexis Kauffmann
* Coordination des développements : Pôle de compétences Logiciels Libres

### Accompagnement et support

Assistance de niveau 1 aux utilisateurs : Assistance de votre académie

Assistance de niveau 2 et modération : DRANE site de Lyon

Assistance technique de niveau 3 et maintenance : Pôle de compétences Logiciels Libres, DSI Académie de Dijon, 2 G rue du général Delaborde, 21000 Dijon

* mail de contact : forge-commun-numeriques@ldif.education.gouv.fr
* forum Tchap : https://www.tchap.gouv.fr/#/room/!fnVhKrpqraWfsSirBK

### Hébergement : 

Le service de la Forge des communs numériques éducatifs est hébergé par AVENIR TELEMATIQUE situé 21 avenue de la Créativité, 59650 Villeneuve d’Ascq en France. 

Le service de la Forge des communs numériques éducatifs fait l’objet d’un traitement de données à caractère personnel mis en œuvre par le ministre de l’Éducation nationale et de la jeunesse (110 Rue de Grenelle 75007 Paris) pour l’exécution d’une mission d’intérêt public au sens du e) de l’article 6 du règlement général (UE) 2016/679 du Parlement européen et du Conseil du 27 avril 2016 sur la protection des données (RGPD).

Le ministère de l’Éducation nationale et de la Jeunesse s’engage à traiter vos données à caractère personnel dans le respect de la loi n°78-17 du 6 janvier 1978 modifiée relative à l'informatique, aux fichiers et aux libertés et du RGPD.

Les indications précises relatives au traitement des données à caractère personnel sont disponibles dans le document [Protection et traitement des données à caractère personnel](https://).

## Données personnelles - Forge des communs numériques éducatifs

Le service constitue un traitement de données à caractère personnel mis en œuvre par le ministre de l’Éducation nationale et de la Jeunesse (110 Rue de Grenelle 75007 Paris) pour l’exécution d’une mission d’intérêt public au sens du e) de l’article 6 du règlement général (UE) 2016/679 du Parlement européen et du Conseil du 27 avril 2016 sur la protection des données (RGPD).

Le ministère de l’Éducation nationale et de la Jeunesse s’engage à traiter vos données à caractère personnel dans le respect de la loi n°78-17 du 6 janvier 1978 modifiée relative à l'informatique, aux fichiers et aux libertés et du RGPD.

Les catégories de données à caractère personnel concernées et leurs durées de conservation sont : 
- les données de fréquentation : 12 mois 
- les données techniques : journalisation des connexions 12 mois glissants
- les données permettant la connexion et concernant le profil de l’utilisateur : 2 mois après la désinscription de l’utilisation ou à l’arrêt du service.
- les données produites par les utilisateurs identifiés ou sous leurs responsabilités

Les catégories de destinataires des données à caractère personnel sont : 
- les administrateurs système habilités ;
- les administrateurs fonctionnels habilités ;
- les administrateurs du pôle de supervision (analyse statistique) ;
- chacun des utilisateurs identifiés pour se propres données ;
- les autres utilisateurs identifiés (lorsque les données sont restreintes à ce public) ;
- le grand public (dont les élèves et les parents) pour toutes les données rendues publiques.

Vous pouvez accéder aux données vous concernant et exercer vos droits d’accès, d’information, de rectification, d’effacement, de limitation, d’opposition que vous tenez des articles 15, 16, 18 et 21 du RGPD, par courriel auprès du recteur d'académie dont dépend votre établissement. Pour plus d’informations, vous pouvez consulter l’adresse suivante : https://www.education.gouv.fr/les-regions-academiques-academies-et-services-departementaux-de-l-education-nationale-6557

De la même manière, vous pouvez exercer les droits prévus à l’article 85 de la loi n° 78-17 du 6 janvier 1978 relative à l’informatique, aux fichiers et aux libertés.

Pour toute question sur le traitement de vos données dans ce dispositif, vous pouvez contacter le délégué à la protection des données du ministère de l’éducation nationale et de la jeunesse : 
- à l’adresse électronique suivante : dpd@education.gouv.fr
- via le formulaire de saisine en ligne : http://www.education.gouv.fr/pid33441/nous-contacter.html#RGPD
- ou par courrier en vous adressant au : Ministère de l'Éducation nationale et de la Jeunesse - À l'attention du délégué à la protection des données (DPD) - 110, rue de Grenelle - 75357 Paris Cedex 07

Si vous estimez, après nous avoir contactés, que vos droits ne sont pas respectés ou que ce dispositif n’est pas conforme aux règles de protection des données, vous pouvez adresser une réclamation auprès de la Commission nationale de l’informatique et des libertés (CNIL).

### Données statistiques, traçabilité

Lors de leur navigation sur le site, les internautes laissent des traces informatiques. Cet ensemble d'informations est recueilli à l'aide d'un témoin de connexion appelé cookie. 

Afin de mesurer l'audience des services et contenus éditoriaux, nous utilisons une solution gestionnaire des statistiques : MATOMO.

### Cookies

Les données générées par les cookies sont transmises au Pôle national Supervision de Nancy. Les données sont hébergées sur des serveurs du ministère de l’éducation nationale.

Le cookie d’analyse de fréquentation défini sur les deux sites assure la remontée d’information et permet les extractions statistiques relatives au profil des internautes : équipement, navigateur utilisé, origine géographique des requêtes limitées aux pays, date et heure de la connexion, navigation sur le site, fréquence des visites, adresse IP anonymisée par suppression des deux derniers octets de l’adresse IP.

Le cookie d’analyse a une durée de vie de 13 mois dans vos navigateurs.

Les données collectées de façon anonyme sont conservées 12 mois.

La configuration de notre logiciel de mesure de l'audience a été effectuée en suivant les recommandations la CNIL, et nous implémentons la fonctionnalité “Do Not Track” réglable dans les navigateurs.

Les cookies sont déposés sur l’équipement de l'internaute. Ce sont : 
- les cookies fonctionnels permettent de personnaliser l'utilisation du site (par exemple mémoriser un mode de présentation) ;
- les cookies destinés à la mesure d'audience. Ils ne collectent pas de données personnelles. Les outils de mesures d'audience sont déployés afin d'obtenir des informations sur la navigation des visiteurs. Ils permettent notamment de comprendre comment les utilisateurs arrivent sur un site et de reconstituer leur parcours. 

Vous avez la possibilité de refuser l'enregistrement de ces données en modifiant la configuration du navigateur de votre équipement ou de les supprimer sans que cela ait une quelconque influence sur votre accès aux pages du site et aux services. 

Vous pouvez vous opposer à l’enregistrement de cookies en configurant les paramètres de votre navigateur

### Journalisation

Les journaux de connexion contenant des adresses IP et d’autres données à caractère personnel sont conservés pour une période de 12 mois glissants pour répondre aux exigences légales c’est à dire au maximum pour une durée de 12 mois après l’extinction des services.

## Conditions générales d'utilisation - Forge des communs numériques éducatifs

### 1. Objet

Toute Personne physique, Utilisateur identifié pour accéder au service de la Forge des communs numériques éducatifs à l’adresse forge.apps.education.fr/, accepte d'être lié par les termes des Conditions Générales d'Utilisation prévues par cet accord.

Tout accès et/ou utilisation du site suppose l'acceptation et le respect de l'ensemble des termes des présentes Conditions Générales d’Utilisation et leur acceptation inconditionnelle.

Ces Conditions Générales d’Utilisation constituent donc un contrat entre le Ministère en charge de l’éducation nationale et l'Utilisateur.

Dans le cas où l'Utilisateur identifié ou non ne souhaite pas accepter tout ou partie des présentes conditions générales, il lui est demandé de renoncer à tout usage du service de SSO et des services associés qui sont disponibles.

### 2. Définitions 

++Condition Générale d’Utilisation (« CGU »)++ : désigne les présentes conditions d’utilisations du service de la Forge des communs numériques éducatifs.

++Éducation nationale++ : désigne à la fois l’administration centrale et les académies auxquels sont rattachés les Utilisateurs du Service (identifiés pour les personnels de l’éducation et anonyme pour les élèves principalement).

++Service++ : désigne le service fourni par Ministère en charge de l’Éducation nationale et ses sous-traitants, accessible par le biais du service de la Forge des communs numériques éducatifs.

++Site++ : désigne le site ou le service de la Forges des communs numériques éducatifs accessible depuis l’adresse https://forge.apps.education.fr/.

++Utilisateur identifié++ : désigne toute personne physique qui utilise le service de la Forges des communs numériques éducatifs, service dont l’accès est réservé à l’Utilisateur sur la base d’un identifiant et d’un mot de passe confidentiel lui permettant d’accéder aux pleines fonctionnalités des autres services. 

### 3. Présentation du site

Ce service permet à l’utilisateur identifié d’accéder à une forge logicielle, reposant sur le logiciels libre GitLab Community Edition, pour y déposer, collaborer et partager du code et du contenu à vocation pédagogique. Il est réservé en écriture aux personnels de l’Éducation nationale et aux partenaires inscrits pour une durée limitée et renouvelable sur un annuaire externe des comptes invités. L’accès en lecture aux ressources de la Forge des communs numériques éducatifs peut être public. 

### 4. Conditions d'accès générales

Tous les coûts afférents à l'accès au Site, que ce soient les frais matériels, logiciels ou d'accès à internet sont exclusivement à la charge de l'Utilisateur. Il est seul responsable du bon fonctionnement de son équipement informatique ainsi que de son accès à Internet.

Le Ministère en charge de l’Éducation nationale met en œuvre tous les moyens raisonnables à sa disposition pour assurer un accès de qualité au Site, mais n'est tenu à aucune obligation d'y parvenir.

Le Ministère en charge de l’Éducation nationale ne peut, en outre, être tenu responsable de tout dysfonctionnement du réseau ou des serveurs ou de tout autre événement échappant au contrôle raisonnable, qui empêcherait ou dégraderait l'accès au Service.

Le Ministère en charge de l’Éducation nationale ne peut, en outre, être tenu responsable de tout traitement des données personnelles ne relevant pas des dispositions e) du 1 de l’article 6 du RGPD publié sur la Forge des communs numériques éducatifs.

Le Ministère en charge de l’Éducation nationale se réserve la possibilité d'interrompre, de suspendre momentanément ou de modifier sans préavis l'accès à tout ou partie du Service, afin d'en assurer la maintenance, ou pour toute autre raison, sans que l'interruption n'ouvre droit à aucune obligation ni indemnisation.

### 5. Règles d'utilisation du service et engagement de l'utilisateur

L’accès aux fonctionnalités du service de la Forge des communs numériques éducatifs est réservé aux personnes travaillant pour l’Éducation nationale et aux comptes invités par un accès contrôlé par un couple identifiant/mot de passe (Utilisateur identifié).

L’Utilisateur s’engage à ne pas : 
- Réaliser toute action ou omission dont on pourrait raisonnablement s'attendre à ce qu'elle (i) perturbe ou compromette l'intégrité ou la sécurité du Service ou la vie privée de tout autre utilisateur du Service ou (ii) cause un dommage imminent et matériel au Service, ou à tout autre utilisateur du Service ;
- Utiliser le site : 
    - pour tout but frauduleux, criminel, diffamatoire, de harcèlement ou délictueux, ou pour participer à ou promouvoir toute activité illégale ;
    - pour enfreindre, violer ou porter atteinte à la propriété intellectuelle, à la vie privée ou à d'autres droits, ou pour détourner la propriété de tout tiers ;
    - de transmettre, distribuer ou stocker tout matériel contenant des virus, des bombes logiques, des chevaux de Troie, des vers, des logiciels malveillants, des logiciels espions ou des programmes ou matériels similaires ;
    - de transmettre des informations d'identification trompeuses ou inexactes dans l'intention de frauder, de causer des dommages ou d'obtenir à tort quelque chose de valeur ;
    - de transmettre ou de diffuser du matériel ou des messages non sollicités, ou du matériel ou des messages de marketing ou de promotion non sollicités, ou du "spam", par le biais d'autres moyens, en violation de toute loi ou réglementation applicable ; 
    - de transmettre, distribuer, afficher, stocker ou partager des contenus ou du matériel inappropriés ou obscènes ;
    - tenter de pirater ou d'obtenir un accès non autorisé au Service ou à tout réseau, environnement ou système du Ministère en charge de l’Éducation nationale, ou à tout autre utilisateur du Service ;
    - supprimer, masquer, modifier ou altérer toute marque, logo et/ou avis légal affiché dans ou avec le Service.

Le Ministère en charge de l’Éducation nationale se réserve en tout état de cause la possibilité de suspendre l’accès au Service en cas de non-respect par l’Utilisateur des dispositions des présentes Conditions Générales d’Utilisation, de suspendre l’accès aux contenus rendu publics qu’il aurait déposé et d’engager sa responsabilité devant toute instance et tribunal compétents.

### 6. Propriété intellectuelle

A l’exception des contenus apportés par les Utilisateurs, l’ensemble des éléments qui figurent sur le Service de la Forge des communs numériques éducatifs sont, sauf mention contraire, sous [Licence Ouverte 2.0](https://www.etalab.gouv.fr/licence-ouverte-open-licence/).

Il appartient à l’Utilisateur identifié de choisir lui-même la licence des contenus qu'il publie sur la Forge des communs numériques éducatifs [parmi le catalogue de licences libres proposés](https://www.data.gouv.fr/fr/pages/legal/licences/). 

Il appartient également à l’Utilisateur identifié, seul responsable de l’ajout de ces contenus, de vérifier préalablement à leur dépôt la titularité des documents ou des composants des documents ajoutés (photographies, audiogrammes, vidéogrammes, illustrations, etc…). 

Si l’Utilisateur identifié décide d’autoriser des élèves (utilisateurs anonymes) à modifier ou à ajouter des documents, il est lui-même considéré comme responsable des publications réalisées sur le service et doit organiser la modération à posteriori de ces modifications ou ajouts le cas échéant.

En cas d’utilisation illégale ou non autorisée du Service, le Ministère en charge de l’Éducation nationale se réserve le droit prendre toute mesure adéquate qu’il estime nécessaire et, le cas échéant, d’intenter toute action administrative ou en justice appropriée, et/ou de signaler l’infraction aux autorités judiciaires et de police.

### 7. Sécurité

Compte tenu de l’évolution des technologies, des coûts de mise en œuvre, de la nature des données à protéger ainsi que des risques pour les droits et libertés des personnes, le Ministère en charge de l’Éducation nationale met en œuvre toutes les mesures techniques et organisationnelles appropriées afin de garantir la confidentialité des données à caractère personnel collectées et traitées et un niveau de sécurité adapté au risque.

Dans le cas où le Ministère en charge de l’Éducation nationale confie les activités de traitement de données à des sous-traitants, ces derniers seront notamment choisis pour les garanties suffisantes quant à la mise en œuvre des mesures techniques et organisationnelles appropriées, notamment en termes de fiabilité et de mesures de sécurité.

### 8. Limitation de responsabilité

L’accès par l’Utilisateur au Service nécessite l'utilisation d'un Identifiant et d'un mot de passe gérés par le Service.

Le mot de passe est personnel et confidentiel.

L’Utilisateur s'engage à conserver secret son mot de passe et à ne pas le divulguer sous quelque forme que ce soit.

Les liens hypertextes présents sur le Site et renvoyant à un site Internet tiers ne sauraient engager la responsabilité du Ministère en charge de l’Éducation nationale.

Le Ministère en charge de l’Éducation nationale n’exerçant aucun contrôle et n’ayant aucune maîtrise sur le contenu de tout site tiers, vous y accédez sous votre propre responsabilité.

Le Ministère en charge de l’Éducation nationale ne saurait en aucun cas être tenu responsable du contenu ainsi que des produits ou services proposés sur tout site tiers.

### 9. Modification des CGU

Le Ministère en charge de l’Éducation nationale se réserve le droit de modifier les termes, conditions et mentions du présent contrat à tout moment.
Il est ainsi conseillé à l'Utilisateur de consulter régulièrement la dernière version des Conditions Générales d'Utilisation disponible sur le Site.

La version actuellement en vigueur est datée du 29 mars 2024.

### 10. Durée et résiliation

Le présent contrat est conclu à compter de l’inscription au Service qui vaut acceptation sans réserve des présentes Conditions Générales d’Utilisation par l’utilisateur.

Le contrat prend fin soit à l’occasion de la suppression du service par le Ministère en charge de l’Éducation nationale soit lorsque l’utilisateur se désinscrit.

La résiliation correspond à la suppression du service par le Ministère en charge de l’Éducation nationale. Elle peut intervenir sans préavis de l’utilisateur et n'ouvre droit à aucune obligation ni indemnisation à la charge du Ministère en charge de l’Éducation nationale et au profit de l’Utilisateur.

### 11. Dispositions diverses

Si une partie des CGU devait s'avérer illégale, invalide ou inapplicable, pour quelque raison que ce soit, les dispositions en question seraient réputées non écrites, sans remettre en cause la validité des autres dispositions qui continueront de s'appliquer aux Utilisateurs.

Les présentes CGU sont soumises au droit administratif français.