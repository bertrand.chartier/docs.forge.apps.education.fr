# CENTRE DE DOCUMENTATION DE LA FORGE DES COMMUNS NUMÉRIQUES ÉDUCATIFS

Ce projet est la page d'arrivée de la forge des communs numériques éducatifs. Elle sert de point d'entrée sur l'outil et de centre de documentation.

Pour les questions sans réponses, ne pas hésitez à créer une demande, soit par courriel sur forge-apps+guichet+docs-support-623-issue-@phm.education.gouv.fr, soit en ouvrant un ticket sur https://forge.apps.education.fr/docs/support

Rendu du site : https://docs.forge.apps.education.fr/


## Construire le site localement

1. Dans un [virtualenv](https://docs.python.org/3/library/venv.html), installer les dépendances.

       python -m pip install requirements.txt

2. Au choix :

   a. Construire le site web :

          mkdocs build

      Le site est alors construit dans le répertoire ``site``.

   b. Lancer un serveur web :

          mkdocs serve

      Le site est alors visible à l'adresse : http://localhost:8000
